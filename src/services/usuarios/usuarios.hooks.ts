import { default as feathers, HookContext } from '@feathersjs/feathers';

export default {
  before: {
    all: [],
    find: [],
    get: [
      async (context: HookContext) => {
        //now é igual bloqueio?
        //  let bloqueiosUser = Bloqueios.default(context.app)
        //   console.log("Hook: "+bloqueiosUser)

        //  if (context.result.data_bloqueio < new Date())
        //     context.result.bloqueado = true

        return context;
      }
    ],
    create:
      [async (context: HookContext) => {
        context.data.data_criacao = new Date();
        context.data.data_atualizacao = new Date();
        return context;
      }]
    ,
    update: [
      async (context: HookContext) => {
        context.data.data_atualizacao = new Date();       
        return context;
      }
    ],
    patch: [
      async (context: HookContext) => {
        if (context.data.nome || context.data.nascimento || context.data.data_ingresso_ifmt ||
          context.data.data_ingresso_campus || context.data.campus_atual){
            context.data.aprovacao_dados = false
          }
        context.data.data_atualizacao = new Date();
        return context;
      }
    ],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
