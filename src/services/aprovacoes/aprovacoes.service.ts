// Initializes the `Aprovacoes` service on path `/aprovacoes`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Aprovacoes } from './aprovacoes.class';
import createModel from '../database';

import hooks from './aprovacoes.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'aprovacoes': Aprovacoes & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/aprovacoes', new Aprovacoes(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('aprovacoes');

  service.hooks(hooks);
}
