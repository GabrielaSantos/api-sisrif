// Initializes the `Area` service on path `/area`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Area } from './area.class';
import createModel from '../database';
import hooks from './area.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'area': Area & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/area', new Area(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('area');

  service.hooks(hooks);
}
