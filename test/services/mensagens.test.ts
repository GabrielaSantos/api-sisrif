import assert from 'assert';
import app from '../../src/app';

describe('\'Mensagens\' service', () => {
  it('registered the service', () => {
    const service = app.service('mensagens');

    assert.ok(service, 'Registered the service');
  });
});
