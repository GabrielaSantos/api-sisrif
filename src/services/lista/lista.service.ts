import { Lista } from './lista.class';
// Initializes the `Inscricao` service on path `/inscricao`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import createModel from '../database';

import hooks from './lista.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'lista': Lista & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/lista', new Lista(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('lista');

  service.hooks(hooks);
}
