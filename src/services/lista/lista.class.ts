import { Params, Id, Application, ServiceMethods } from '@feathersjs/feathers';
import { KnexServiceOptions } from 'feathers-knex';

export class Lista implements Omit<ServiceMethods<any>, 'find'> {
  knex: any

  constructor(options: Partial<KnexServiceOptions>, app: Application) {
    this.knex = options.Model
  }

  async find(params: any) {
    console.log(params.query['idCampus'])
    
    let result = (await this.knex.raw('call listas(' + params.query['idCampus'] +','+ params.query['idCargo'] +');'))[0][0]
    return result;
  }

  async get(id: Id, params: Params) {
    console.log("GET: " + params)
  }

  setup(app: Application, path: string) { }
}
