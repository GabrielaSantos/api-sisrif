import assert from 'assert';
import app from '../../src/app';

describe('\'Inscricao\' service', () => {
  it('registered the service', () => {
    const service = app.service('inscricao');

    assert.ok(service, 'Registered the service');
  });
});
