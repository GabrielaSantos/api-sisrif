import assert from 'assert';
import app from '../../src/app';

describe('\'CampusProximo\' service', () => {
  it('registered the service', () => {
    const service = app.service('campus-proximo');

    assert.ok(service, 'Registered the service');
  });
});
