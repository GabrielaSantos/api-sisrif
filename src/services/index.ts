import { Application } from '../declarations'
import usuarios from './usuarios/usuarios.service'
import campus from './campus/campus.service'
import campusProximo from './campus-proximo/campus-proximo.service'
import area from './area/area.service'
import cargo from './cargo/cargo.service'
import inscricao from './inscricao/inscricao.service'
import vagas from './vagas/vagas.service'
import aprovacoes from './aprovacoes/aprovacoes.service'
import reprovacoes from './reprovacoes/reprovacoes.service'
import bloqueios from './bloqueios/bloqueios.service'
import log from './log/log.service'
import lista from './lista/lista.service'
import mensagens from './mensagens/mensagens.service';
import { login } from './login'
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application) {
  app.configure(campus)
  app.configure(usuarios)
  app.configure(campusProximo)
  app.configure(area)
  app.configure(cargo)
  app.configure(vagas)
  app.configure(inscricao)
  app.configure(aprovacoes)
  app.configure(reprovacoes)
  app.configure(bloqueios)
  app.configure(log)
  app.configure(lista)
  app.configure(mensagens);

  login(app)
}
