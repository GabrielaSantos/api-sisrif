// Initializes the `CampusProximo` service on path `/campus-proximo`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { CampusProximo } from './campus-proximo.class';
import createModel from '../database';

import hooks from './campus-proximo.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'campus-proximo': CampusProximo & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/campus-proximo', new CampusProximo(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('campus-proximo');

  service.hooks(hooks);
}
