import { HookContext } from "@feathersjs/feathers";
import moment from 'moment'

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      async (context: HookContext) => {
        context.data.data_criacao = new Date();
        context.data.data_atualizacao = new Date();    

        if(context.data.data_liberacao == null) {
          var agora = moment()
          var tempo_bloqueio = moment.duration(90, 'd');              
          context.data.data_liberacao = agora.add(tempo_bloqueio).format('YYYY-MM-DD')
        }
        
        return context;
      }
    ],
    update: [
      async (context: HookContext) => {
        context.data.data_atualizacao = new Date();
        return context;
      }
    ],
    patch: [
      async (context: HookContext) => {
        context.data.data_atualizacao = new Date();
        return context;
      }
    ],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
