import assert from 'assert';
import app from '../../src/app';

describe('\'Area\' service', () => {
  it('registered the service', () => {
    const service = app.service('area');

    assert.ok(service, 'Registered the service');
  });
});
