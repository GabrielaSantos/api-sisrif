// Initializes the `Reprovacoes` service on path `/reprovacoes`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Reprovacoes } from './reprovacoes.class';
import createModel from '../database';

import hooks from './reprovacoes.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'reprovacoes': Reprovacoes & ServiceAddons<any>;
  }
}

export default function (app: Application) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/reprovacoes', new Reprovacoes(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('reprovacoes');

  service.hooks(hooks);
}
