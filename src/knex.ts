import knex from 'knex';
import { Application } from './declarations';

export default function (app: Application) {
  const db = knex({
    client: 'mysql',
    connection: {
      host : '127.0.0.1',
      user : 'root',
      password : 'root',
      database : 'sisrif'
    }
  });

  app.set('sisrif', db);
}
