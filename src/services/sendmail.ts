const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: "sisrif@ifmt.edu.br",
        pass: "@rVlnkq6"
    },
    tls: { rejectUnauthorized: false }
});


function sendMail(destination: string, subject: string, body: string) {
    const mailOptions = {
        from: 'sisrif@ifmt.edu.br',
        to: destination,
        subject: subject,
        html: body
    };

    transporter.sendMail(mailOptions, function (error: any, info: any) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email enviado: ' + info.response);
        }
    });
}

export { sendMail }